// The level of agreement between the Mages. When this reaches 1.0, they will
// get insane and start the Massacre.
VAR agreement = 0.0

// How much the agreement grows per hour.
CONST AGREEMENT_PER_HOUR = 0.05

// Hours elapsed since the start of the game (in game clock).
VAR hoursElapsed = 0

// Player's skills.
VAR skillStealth = 4
VAR skillWillpower = 4
VAR skillCunning = 4
VAR skillFighting = 4
VAR skillAthletic = 4

// Player's health
CONST MAX_HEALTH = 3
VAR health = MAX_HEALTH

// Boat complete when at 1.0.
VAR boatCompleteness = 0.0

// Amount of of work done per hour.
VAR boatBuildingSpeed = 0.03

// How many of the Mages bases has the player scouted?
VAR numBasesScouted = 0

// Points available to buy stakes (after a skill roll).
VAR pointsAvailable = 0

// Did the player buy each of the possible stakes on a skill roll?
VAR boughtOutcome1 = false
VAR boughtOutcome2 = false
VAR boughtOutcome3 = false
VAR boughtOutcome4 = false
VAR boughtOutcome5 = false

// Things the Player can know or have, which helps to create hostility between
// Mages. `0` means "not known or owned". `1` means "known or owned". `2` means
// "already used".
VAR hasLifeMushroom = 0
VAR hasEwingVoodooDoll = 0

// Things that create new quests
VAR hasSourceOfQuests = false

// Show quests that depend on `hasSourceOfQuests`?
VAR showAllQuests = false

// Which boat building help information the player knows.
VAR bbhToolsLocation = false
VAR bbhPitchBarrel = false
VAR bbhFallenTrees = false
VAR bbhRopes = false


=== function printHealth()

(Current health: {health}/{MAX_HEALTH}.)


=== function passTime(hours)

~ hoursElapsed += hours
~ agreement += hours * AGREEMENT_PER_HOUR
{ agreement > 1.0:
    ~ agreement = 1.0
}
~ boatCompleteness += hours * boatBuildingSpeed
{ boatCompleteness > 1.0:
    ~ boatCompleteness = 1.0
}


=== function reduceAgreement(multiplier)

~ temp before = agreement

~ agreement -= 0.1 * multiplier
{ agreement < 0.0:
    ~agreement = 0.0
}

# CLASS: feedback
Level of agreement between the Mages went from {INT(before * 100)}% down to {INT(agreement * 100)}%.
