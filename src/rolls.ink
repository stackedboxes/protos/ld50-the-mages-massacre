// Rolls for `skill`, returns the number of "points" rolled (that is, the number
// of stakes that the player can buy).
=== function roll(skill)

{ roll == 1:
    # CLASS: help
    <strong>This is your fist skill roll, so here's some help.</strong> You get to roll one dice per skill level you have, and each roll has 50% chance of giving you one <i>point</i>. Right after the roll, you'll see what's at stake on this roll, that is, what are the possible <i>outcomes</i> of it -- both good and bad. You can then use the points you got to buy outcomes, so that good ones happen and bad ones don't. Some outcomes are marked with ❗, meaning they are very important: failing to buy them will cause other outcomes you might buy to be ignored. So, a good strategy is to start by buying the marked outcomes, then buying the other ones as you please. You don't have to spend all your points on a roll, but you cannot keep remaining points for later.
}

~ temp numDice = 0
~ boughtOutcome1 = false
~ boughtOutcome2 = false
~ boughtOutcome3 = false
~ boughtOutcome4 = false
~ boughtOutcome5 = false

{
    - skill == "stealth":
        ~ numDice = skillStealth
    - skill == "willpower":
        ~ numDice = skillWillpower
    - skill == "cunning":
        ~ numDice = skillCunning
    - skill == "fighting":
        ~ numDice = skillFighting
    - skill == "athletic":
        ~ numDice = skillAthletic
    - else:
        Error, unknown skill "{skill}". Sorry, this is a bug. 😢
}


~ rollThisManyDice(numDice)
You rolled {numDice} dice for <i>{skill}</i> and got <>

{
    - pointsAvailable == 0:
        no points. <>
        { shuffle:
            - Though luck!
            - Ouch!
            - Baaad!
            - Oh my!
        }


    - pointsAvailable == 1:
        just one point.
    - else:
        {pointsAvailable} points.
}

{ pointsAvailable == numDice:
    { shuffle:
        - <> Lucky strike!
        - <> Hurray!
        - <> Yeah!
        - <> That's the way!
    }
}


// Helper for `roll()`. Apparently Ink doesn't give anything like a `for` loop
// for iteration, so let's recurse.
=== function rollThisManyDice(dice)
    {
        - dice == 0:
            ~ return
    }
    ~ pointsAvailable += RANDOM(0, 1)
    ~ return rollThisManyDice(dice - 1)


=== function printZeroRoll()

That roll was {~disastrous|beyond bad|catastrophic|cataclysmic|tragic|terrible}! You cannot buy any stakes.


=== function printPointsAvailable()

{ pointsAvailable > 0:  You still have {pointsAvailable} point{pointsAvailable > 1:<>s}.}
