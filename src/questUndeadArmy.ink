=== questUndeadArmy

# CLEAR

Your intel tell there's a small army of undead in the vicinities of the Red Brick village. The plan is to defeat this army and make it look like an attack from Ewing's earth elementals. This shall piss Nikki, the Necromancer, off.

~ passTime(2)

You head to the reported army location and spot a group of a dozen of undead. Not super easy, but not that hard either. Just sneak in and hit them hard on the head. Standard undead killing procedure.

* [Attack! (Roll <i>fighting</i>)] -> attack
* [Give up, it's too risky] -> theHub

= attack

- ~ roll("fighting")

-> attackStakes(true) ->

~temp hits = 0

{ not boughtOutcome2:
    ~ hits += 1
}
{ not boughtOutcome3:
    ~ hits += 1
}
{ not boughtOutcome4:
    ~ hits += 1
}

You hold your dagger tightly and approach the undead. You kill several of them silently. Everything is going well until misfortune decides to join the fray. A vulture lands right beside you, making noise and catching the attention of the undead.

A more savage fight ensues. You get surrounded,

{
    - hits == 0:
        <> but miraculously get out of the circle of undead without a scratch!
    - else:
        <> unfortunately, and suffer damage.
        ~ health -= hits
        -> checkIfAlive ->
        <> Despite the wounds, you still find a way to get out of the circle of undead.
}

{
    - boughtOutcome1:
        After that you refocus, attack the remaining foes, and emerge victor!
        * [Yeah!] -> falseFlag
    - else:
        You are scared. The undead charge against you, and your only reaction is to run away.
        * [Sigh] -> theHub
}

= attackStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [Defeat the undead.]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> attackStakes(false)
    * [Avoid being hit. {printHealth()}]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> attackStakes(false)
    * [Avoid being hit again. {printHealth()}]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> attackStakes(false)
    * [Avoid being hit once more. {printHealth()}]
      ~ boughtOutcome4 = true
      ~ pointsAvailable -= 1
      -> attackStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= falseFlag

Alright, the risky part of the quest is over and was a success. Now, how to make it look like an attack by Ewing, the Elementalist?

~ temp smarts = 0

{ hasLifeMushroom:
    ~ smarts += 1
    You start off by placing that Life Mushroom you found on Ewing's room in a prominent place. But what else can you do?
}

* [Think (Roll <i>cunning</i>)]
- ~ roll("cunning")
-> falseFlagStakes(true) ->

{ boughtOutcome1:
    ~ smarts += 1
}
{ boughtOutcome2:
    ~ smarts += 1
}
{ boughtOutcome3:
    ~ smarts += 1
}

You do your best to fake an attack by Ewing, the Elementalist. You hide and wait for Nikki to arrive ane watch her reaction.

* [Wait]

~ passTime(1)

- Nikki, the Necromancer arrives after an hour. You see her walking around the immovable undead. <>

{
    - smarts == 0:
        "What the heck happened here?," she says, genuinely not understanding. Your efforts were not profitable.
        * [Too bad!] -> theHub
    - smarts == 1:
        "That's odd. Could Ewing be behind this attack?" She's not super impressed, but you got something.
        ~ reduceAgreement(smarts)
        * [Better than nothing!] -> theHub
    - smarts == 2:
        "I can't believe! Ewing did this?" She <i>is</i> impressed.
        ~ reduceAgreement(smarts)
        * [Good!] -> theHub
    - smarts >= 3:
        "Ewing! Ewing! Ewing!," she screams uncontrollably while jumping up and down. You did a hell of job here!
        ~ reduceAgreement(smarts)
        * [Great!] -> theHub
}

= falseFlagStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [Scatter earth over the battlefield]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> falseFlagStakes(false)
    * [Put some rocks in the undead wounds]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> falseFlagStakes(false)
    * [Fake some footsteps in the shape of elemental feet]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> falseFlagStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->



