=== endMassacreOnly

While you think about to do next, you feel like you are shivering. Is this fear? You know this is a dangerous mission, but you never felt like this before.

That's when you realize it's not you shivering, it's the ground trembling!

It's like if a mountain is growing out of the soil, right in front of you. It starts to get human shaped, you are confused.

"It's an earth elemental, stupid!"

You turn towards the voice. It's Ewing, the Elementalist, laughing madly.

* [Get away!]

- You run way, as fast as you can, for as long as you can. You leave the elemental behind, but you see an army of undead coming from the other side. You hesitate. When you look back, you see the elemental again, throwing stone darts at you.

You dodge the first dart, but the second one bites part of your leg off. Bleeding and in pain, you roll on the ground, taking and throwing stones at the approaching undead. That's hopeless, you are soon surrounded by Nikki's army.

The Mages Massacre has come, and you were only the first victim.

# CLASS: end
THE END
-> END
