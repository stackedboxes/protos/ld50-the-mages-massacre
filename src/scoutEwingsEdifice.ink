=== scoutEwingsEdifice

# CLEAR

~ numBasesScouted += 1
~ passTime(1)

Describing Ewing's Edifice, the headquarters of the Elementalist, is not easy. We usually call it a cave, but that's not accurate. Its design is unlike a natural cave, as it's full of straight lines. And did I say design? Yes, it was designed and build by Ewing himself, by using his elemental powers to shape the earth and he desired.

From behind some rocks you observe the many Mind Slaves around the cave entrance. None of them seems to be on guard duty, they are just going in and out between the cave and the road to the City.

"Mules," you think, referring to those poor souls whose life is reduced to carrying all the stuff the Mages need.

After a while you notice a large number of heinous creatures leaving the cave: that smaller, nimbler variation of earth elementals that Ewing uses as his personal guard. And, sure enough, you soon spot Ewing himself in the middle of his bizarre escort.

One Elemental stays guarding the cave, the rest follow the Mage to the road. This is your chance to get in.

* [Enter stealthily. (Roll for <i>stealth</i>)] -> enterStealthily
* [Fight the Elemental guard. (Roll for <i>fighting</i>)] -> enterFighting

// - ENTER STEALTHILY ---------------------------------------------
= enterStealthily

~ roll("stealth")

-> enterStealthilyStakes(true) ->

{
    - not boughtOutcome1:
        -> detectedByGuard
    - not boughtOutcome2:
        You go past the guard unseen, but right after that a Mule spots you.
        -> detectedByMule
    - else:
        Nobody notices as you get in.
        ~ passTime(1)
        * [Good] -> maze
}


= enterStealthilyStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [❗Get unnoticed by the Elemental guard.❗]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> enterStealthilyStakes(false)
    * [Get unnoticed by the Mules.]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> enterStealthilyStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= detectedByGuard

You trip into a pebble as you get close to the guard. The Elemental turns its head towards you and a gets to a fighting stance with its rocky arms.

-> fight

// - ENTER FIGHTING  -------------------------------------------

= enterFighting

You courageously approach the Elemental guard.

"Come get some," you say.

In response, it shrieks menacingly.

-> fight

// - FIGHT --------------------------------------------------------
= fight

~ passTime(1)

<> Time to fight!

* [Fight! (Roll <i>fighting</i>)]

- ~ roll("fighting")

-> fightStakes(true) ->

~temp hits = 0

{ not boughtOutcome2:
    ~ hits += 1
}
{ not boughtOutcome3:
    ~ hits += 1
}

{ hits > 0:
    The Elemental hit you good.
    ~ health -= hits
    -> checkIfAlive() ->
    <> You manage to survive, though.
}

{
    - boughtOutcome1:
        You kill the Elemental guard, but with the fight a Mule got aware of your presence.
        -> detectedByMule
    - else:
        Unfortunately, you couldn't defeat the Elemental guard. Your only choice is to run away. You didn't find anything useful in this scout quest.

        * [Sigh] -> theHub
}

= fightStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [❗Defeat the Elemental.❗]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> fightStakes(false)
    * [Avoid being hit. {printHealth()}]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> fightStakes(false)
    * [Avoid being hit again. {printHealth()}]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> fightStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->


// - DETECTED BY MULE ----------------------------------------------
= detectedByMule

~ passTime(1)

"Unidentified serf," says the Mule, "declare your task."

You'll have to use your cunning. You can outsmart a Mind Slave, can't you?

* [I hope so! (Roll <i>cunning</i>)]

- ~ roll("cunning")

-> detectedByMuleStakes(true) ->

{
    - not boughtOutcome1:
        You wanted you could be more convincing: "I am a Mind Slave, you know? Just, uh, doing my Mind Slave things. I am <i>*gasp*</i> harmless, you see?"

        The Mule is not impressed: "Intruder detected! Intruder detected!"

        You immediately hear the sound of a small army of Elementals coming from the darkness of the cave. You decide to run away, before they mince you into pieces.

        You'll be safe, but that was bad scout quest!

        * [Sigh] -> theHub

    - else:
        Fooling a Mind Slave is easy enough for you: "Serf number 41. Cleaning and hygiene."

        You see the Mule relaxing immediately after your response. "Ok," he says.

        {
            - boughtOutcome2:
                You take the chance to ask: "Need to clean <strong>tools</strong>. Mitigate tetanus risk. Need tools location."

                "Tools location: shed in the Yellow Forest."

                ~ bbhToolsLocation = true

                * Thank you!-> detectedByMule2
            - else:
                -> detectedByMule2
        }
}

= detectedByMule2

{
    - boughtOutcome3:
        You try to get some {boughtOutcome2: more} help from the Mule: "Need to clean the Master room. Need location."

        { boughtOutcome2:
            And again it <>
          - else:
            It <>
        }

        works: "Second tunnel right, then left, right, and right again."

        Great, this will spare you some search time! You leave, following the directions.

        * [Get into Ewing's room] -> masterRoom
    - else:
        -> maze
}

= maze

You head towards the depths of the cave, looking for Ewings' room. The cave has a number of passages and crannies, a real maze. You explore the Elementalist's lair, carefully, but nonstop.

~ passTime(4)

You lose a good amount of time going around, but eventually you find what you were looking for.

* [Get into Ewing's room] -> masterRoom

= detectedByMuleStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [❗Convince the Mule you are a Mind Slave too.❗]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> detectedByMuleStakes(false)
    * [Discover something useful for the boat builders.]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> detectedByMuleStakes(false)
    * [Find out how to get to Ewing's room.]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> detectedByMuleStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

// - MASTER ROOM ------------------------------------------------
= masterRoom

~ passTime(1)

You enter Ewing's main room. A magic circle on the floor surely marks the point where the earth elemental power lines converge. He selected the location of his base carefully! It's quite dark in here. Is that a small notebook on the desk?

* [Take the notebook.]

- You grab the notebook and browse through it. Looks like this is where he keeps his notes and personal schedule. This will help for sure! <i>A lot!</i>

~ hasSourceOfQuests = true

Knowing that Ewing is not around, you feeling safe to explore the room a little further. And you are glad you did: you spot something interesting on the corner. It's hard to believe, but it must be... they look just like the stories tell: it's a little patch of <strong>Life Mushrooms</strong>! They are said to be effective against necromancy and were thought to be extinct.

* [Take a Life Mushroom]

- You take one Life Mushroom. If you can only find a way to give it to Nikki, the Necromancer -- she will hate to know that Ewing is growing them!

~ hasLifeMushroom = 1

{
    - hasEwingVoodooDoll == 1:
        And speaking of hate, this looks like the perfect place to leave that voodoo doll you got at Nikki's crypt.

        * [Leave the voodoo doll]
          You place the voodoo on the magic circle, along with a note saying "I don't need this one anymore, as I have a better and bigger one -- Nikki, the Necromancer". This shall make Ewing more cautious about Nikki.
          ~ hasEwingVoodooDoll = 2
          ~ reduceAgreement(1)
          -> leave
    - else:
        -> leave
}

= leave

Satisfied with your scout quest, you find your way out of the cave.

~ passTime(1)

* [Hurray!]

- -> theHub
