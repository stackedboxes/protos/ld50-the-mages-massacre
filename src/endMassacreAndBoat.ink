=== endMassacreAndBoat

You are sitting on a secluded corner if wilderness of the Island, planning your next move. The boat may be ready by bow, shall you return to the boatyard? Perhaps, but you also feel the Mages are almost at the point of starting the Massacre.

Almost?

You hear mad laughs. Three different voices you know well. The Mages Massacre has started!

Your only chance is to reach the boatyard before the Mages, hope that the boat is ready and didn't leave yet! Slim changes, but you have no choice.

Then you feel the power of the mind control spell from three Mages at once! You'll need to resist as you run!

* [Try to resist (Roll <i>willpower</i>)]
  -> resist

// - RESIST ------------------------------------
= resist

~ roll("willpower")
-> resistStakes(true) ->

~temp numResist = 0

{ boughtOutcome1:
    ~ numResist++
}
{ boughtOutcome2:
    ~ numResist++
}
{ boughtOutcome3:
    ~ numResist++
}

{
    - numResist == 3:
        You resisted -- that's some willpower! You dash to the boatyard.
        -> boat(1)
    - numResist == 2:
        Not bad, but you are a bit dizzy. Clumsily, you keeping running to the boatyard.
        -> boat(2)
    - numResist == 1:
        The Mages did a lot of damage to your willpower barrier. You feel confused, but they are not controlling you! You keep going towards the boatyard, very hesitantly.
        -> boat(3)
    - numResist == 0:
        You try to run, but your muscles don't seem to respond to your mind. In fact, you don't feel like you control your mind anymore. Your mind is clean and clear. You calmly walk towards the fiendish creatures the Mages have called to destroy everything and everyone, and remain still as they crush and smash your body.

        # CLASS: end
        THE END
        -> END
}

= resistStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [ Resist Nikki, the Necromancer.]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> resistStakes(false)
    * [ Resist Ewing, the Elementalist.]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> resistStakes(false)
    * [ Resist the synergic effects of the combined spells.]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> resistStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->


// - BOAT -----------------------------------
= boat(difficulty)

You reach the boatyard. You left the Massacre behind, but you can hear and feel the foes are approaching quickly. You immediately see that the Dwellers did what they had to do: they started sailing already. The boat is not docked anymore; <>
{
    - difficulty == 1:
        it has just left the pier. Shouldn't be too hard to swim to it.
    - difficulty == 2:
        it is a moderate distance away from the shore. A good swimmer should be able to reach it.
    - difficulty == 3:
        it is quite a distance from the shore. Swimming to it will require an athletic feat but, again, you have no choice.
}

As you rush towards the pier and prepare to jump into the water, you can hear your friends cheering from the boat.

* [Swim! (Roll <i>athletic</i>)]
  -> swim(difficulty)


// - SWIM -----------------------------------
= swim(difficulty)

~ roll("athletic")

{
    - pointsAvailable == 0:
        You jump into the water, but go directly to the bottom. Are you sure you did know how to swim?
        # CLASS: end
        THE END
        -> END
    - pointsAvailable < difficulty:
        You jump into the water and swim ferociously to save your skin. At first you do a good progress, but your limbs get more and more lethargic as you go. The cheers from the fellow Free Dwellers gradually turn into cries of horror as they see you are drowning.
        # CLASS: end
        THE END
        -> END
    - else:
        You dive majestically, and start swimming with confidence, but conserving your energy. It feels like you are doing every movement right. You soon catch a rope thrown by your friends, who pull you onboard.

        You are received with an ovation.
        -> endBoatOnly.sailing
}
