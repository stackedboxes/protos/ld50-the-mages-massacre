=== endBoatOnly

You hear noises, someone is approaching. Danger! You hide {~behind a tree|behind a rock|among some bushes} and watch, your heart beating fast.

To you surprise, it's a Free Dweller! You get out of your hideout.

"Hey! What brings you here?"

"The Elder sent me! The boat is ready, let's go!"

Shedding tears of relief, you two rush to the boatyard.

* [Run!]

- The boat is ready to sail. You embark as the sailors untie the ropes. The boat start to move, everyone is cheering!

-> sailing

= sailing

When you are already at a safe distance, you can clearly see that the prophecy is being fulfilled in the Island. You hear the screams from armies of undead and the roars of earth elementals. Fire, smoke, the cries of the Mind Slaves being slaughtered.

* [Keep sailing!]

- You spend a week on the sea without any signs of land. Hope is getting lower and lower, food and fresh water are at critical levels. This is looking like a bad ending for the Free Dwellers. And that is looking like... a hill? Is that...? Yes, it is!

* ["Land ho!"]

- # CLEAR

You land on a beautiful beach. What a paradise! Fishes, fruits, birds, wood, a fresh water creek. You Free Dwellers feast on everything the nature offers to you here.

And then you see a woman approaching, walking with confidence and emanating an aura that you can't see but you can feel.

"Welcome to the Atoll!," she says. "Feel at home! I am Toya, the Transmuter, the rightful boss of this place -- never mind what Chlorius, the Conjurer, might say, that fool."

# CLASS: end
THE END
-> END
