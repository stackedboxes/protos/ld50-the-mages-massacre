=== theHub

# CLEAR

{agreement >= 1.0 and boatCompleteness >= 1.0: -> endMassacreAndBoat}
{agreement >= 1.0: -> endMassacreOnly}
{boatCompleteness >= 1.0: -> endBoatOnly}

{
    - theHub == 1 and not wait:
        Inciting conflict is all about information. You don't know much about the Mages at the moment, so you decide to start by scouting their lairs.
    - else:
        It's been {INT(hoursElapsed)} hours since your mission started. The boat is {INT(boatCompleteness * 100)}% complete and the level of agreement among the Mages is at {INT(agreement * 100)}%. Your health is {health} out of {MAX_HEALTH}.
}

{ numBasesScouted == 2 and not boatyardVisit: -> timeToVisitTheBoatyard }

What do you do?

* [Scout the base of Nikki, the Necromancer.] -> scoutNecromancersNest
* [Scout the base of Ewing, the Elementalist.] -> scoutEwingsEdifice
* { showAllQuests } [Quest: False flag attack.] -> questUndeadArmy
* { showAllQuests } [Quest: Stoned meeting.] -> questStonedMeeting
* { showAllQuests } [Quest: Tea for two.] -> questTeaForTwo
+ [Wait.]
    -> wait
+ { boatyardVisit } [Visit the boatyard.]
    ~ passTime(3)
    -> boatyardVisit

= timeToVisitTheBoatyard

{
    - hasSourceOfQuests:
        Your scouting quests went well enough. Analyzing the material you got, you spot some upcoming opportunities to wear away the growing level of agreement between the Mages. You decide to pay a visit to the Elder in the boatyard and share the good news.
    - else:
        After scouting the two Mages lairs, you couldn't find any information suggesting further quests. This is deeply worrying! You decide to pay a visit to the Elder in the boatyard and talk about this.
}

* [To the boatyard!] -> boatyardVisit

= wait

~ passTime(6)

{
    - health == MAX_HEALTH:
        You wait, as if you hadn't anything more important to do.
    - else:
        As you wait, you feel as if your wounds were healing. Maybe because they are.
}

~ health += 1
{ health > MAX_HEALTH:
    ~ health = MAX_HEALTH
}

+ [Alright.] -> theHub
