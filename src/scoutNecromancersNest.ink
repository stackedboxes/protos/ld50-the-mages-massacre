== scoutNecromancersNest

# CLEAR

~ numBasesScouted += 1
~ passTime(1)

The Turtle Hill Cemetery. This is not where the people of the Island bury all their dead, but it is the place where all their dead end up at. Nikki, the Necromancer made home here many centuries ago, and uses her powers to bring here all the people buried elsewhere.

You need to get to the crypt at the top of the hill: the Necromancer's Nest, Nekki's house and headquarters. You see some undead toddling around hill. Undead have terrible senses and are easy to avoid. As long as you don't have dozens of them after you, they are not a real threat.

Easy or not, you need to reach the crypt. Hmm, and maybe even inspect a barrel you see just a little off your optimal path.

* [Go uphill, carefully. (Roll <i>stealth</i>)] -> goUphill

// - GO UPHILL -----------------------------------------
= goUphill

~ roll("stealth")

-> goUphillStakes(true) ->

You conscientiously start your uphill stroll, stepping lightly, breathing lightly, perfectly silent, low profile, avoiding the bones that littler the land.

{
    - boughtOutcome2:
        ~ passTime(1)

        Instead of going directly to the crypt you decide to take a short detour to inspect the barrel you noticed earlier. It's a <strong>barrel of pitch</strong>, no doubt used for torches. It probably rolled down here and nobody noticed or cared to take it back.

        You are ready to resume walking when you realize that pitch is also quite useful for anyone building a boat. There are two undead close by, how could you take the barrel to a safer place? You take a skull and some bones from the ground and throw them to make noises, away from yourself and away from the barrel's path downhill. When the undead are away, attracted by your diversion, you gently push the barrel, which rolls to a reasonably safe point right on the margin of the Snake Creek.

        ~ bbhPitchBarrel = true

        * [Back to the climb!] You resume your ascent. <> -> goUphill2
    - else:
        -> goUphill2
}

= goUphill2

{
    - boughtOutcome1:
        Soon enough you reach the top of the hill.

        * [I knew it was going to be easy] -> crypt
    - else:
        Maybe it was overconfidence, maybe it was ineptitude. Partway through the climb you step on a skull that cracks with more noise you'd expect. The closest undead turns towards you and screams. This in turn catches the attention of two other undead, who also scream. Soon enough, the chain reaction of screaming undead is unstoppable -- and <i>this</i>, my friend, is a real threat.

        Your quest is botched. You run away, without any useful piece of intel about Nikki. {bbhPitchBarrel: At least you secured that barrel of pitch.}

        * [I'll be more careful next time] -> theHub
}

= goUphillStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [Get unnoticed by the undead.]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> goUphillStakes(false)
    * [Inspect barrel]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> goUphillStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= crypt

~ passTime(1)

All quiet up here. You approach the crypt's entrance, and stare at the heavy wooden door that separates you from Nikki's lair. You push the door, which moves surprisingly smoothly and without a sound. Wondering if she Necromancer is in, you enter.

After going down a stairway, you find yourself on a corridor punctuated by nests holding funeral urns. The stench is horrible and the air is uncomfortably cold, but the place is quite tidy, clean floor and torches illuminating the way. You can see there's a room at the end of the corridor, and the doors are open.

As you get closer to the room, you see there's someone in there: Nikki, the Necromancer herself!

"I don't get visits very often, and that's for a reason," she says, raising her hands and gently wiggling her fingers. You feel she's trying to mind control you.

* [Try to resist. (Roll <i>willpower</i>)] -> resist

= resist

~ roll("willpower")

-> resistStakes(true) ->

{
    - boughtOutcome1:
        You feel dazed, but this is really a signal you resisted the spell -- mind-controlled people are said to feel as if thoughts just came to their minds clean and clear. -> resisted
    - else:
        You concentrate, trying to resist to the mind control spell. Your mind starts to feel more and more relaxed, you notice some thoughts starting to form out of nowhere. You are failing to resist!

        In an heroic effort, you manage to tur around and dash back through the corridor, the stairway and door. You don't care for undead or anything, you just run and run.

        Luckily, you escape to a safe place. That was a not a good scouting quest, but it sure id good to be alive.

        * [That was close!] -> theHub
}

= resistStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [❗Resist mind control❗]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> resistStakes(false)
    * [Talk to Nikki]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> resistStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= resisted

What next? You must think quickly. Look around. Nikki's desk is at arms reach. Is that a calendar covered in scribbles? Yes it is. Maybe she keeps her appointments there?

* [Grab the calendar]

- You grab Nikki's calendar, hoping it will help you on your mission.

~ hasSourceOfQuests = true

{
    - boughtOutcome2:
        Quick, what else? Talk to Nikki, the Necromancer? Impossible, you are still dazzled by the mind control spell. Since you can't articulate anything deep, you just spit out the first words that come to your mind:

        "You stink, witch! That's why you don't get many visits, isn't it? Hahaha!"

        Maybe mocking a powerful Mage wasn't a good idea, after all. She is angry. Like, <i>real</i> angry. She takes a candlestick and throws it at you. You dodge, back off, and start to run away.

        You dash through the corridor as objects fly past yourself, some hitting, some missing. A rather curious one lands just ahead of you. A doll. In the shape of Ewing, the Elementalist. A <strong>voodoo doll</strong>! You skilfully grab it without loosing much time and go upstairs.

        ~ hasEwingVoodooDoll = 1

        * [Keep fleeing!] -> beforeGoingAway
    - else:
        You already got what you were looking for, so you don't think twice and start fleeing. Skeletons are getting out of the funerary urns, but you are much faster than they are and reach the door at the top of the stairway with but a few scratches.

        * [Keep fleeing!] -> beforeGoingAway
}

= beforeGoingAway

~ passTime(1)

Once outside, you quickly scan the hill for a path free of undead. <>

{
    - hasLifeMushroom == 1:
        Ouch, you almost forgot the Life Mushroom! You throw it through the door yelling "This is a gift from your not-so-good-friend Ewing!" Without loosing any more time, you take a deep breath of fresh air and rush downwards to safety.
        ~ hasLifeMushroom = 2
        ~ reduceAgreement(1)
        * [That was intense!] -> theHub
    - else:
        You take a deep breath of fresh air and rush downwards to safety.
        * [That was intense!] -> theHub
}
