=== boatyardVisit

# CLEAR

// Confusing names here. Means "if has not yet seen the `firstBoatyardVisit` stitch.
{ not firstBoatyardVisit:
    -> firstBoatyardVisit ->
  - else:
    You are at the boatyard.
}

+ [Leave] -> theHub
* {bbhToolsLocation} [Tell about the tools in the Yellow Forest shed.] -> tellToolsLocation
* {bbhPitchBarrel} [Tell about the pitch barrel.] -> tellPitchBarrel
* {bbhFallenTrees} [Tell about the fallen trees.] -> tellFallenTrees
* {bbhRopes} [Tell about Quentin's Rope Shop.] -> tellRopes


// - FIRST VISIT ----------------------------------------------
= firstBoatyardVisit

~ passTime(1)

You approach the boatyard. Your fellow Free Dwellers are busy at work, but you still don't see anything resembling a boat.

{
    - hasSourceOfQuests:
        "Elder, the mission is going well. I broke into the Mages' lairs and found some good intel. I am ready to start the second phase of the mission and cause some real animosity!"

        "Good, but remember the clock is ticking. Keep working -- we boat builders will!"

        -> endOfFirstBoatyardVisit
    - else:
        "Sad news, Elder! I scouted the Mages' lairs but didn't find any good intel. I am desperate!"

        "Let's take some workers and send them to scout the Island. This will delay the boat construction, but we need information."

        * [So be it!] -> sendWorkersToScout
}

= sendWorkersToScout

The Elder musters a group of workers and explains the situation. They leave, and you hope they can find what you couldn't.

~ boatBuildingSpeed = boatBuildingSpeed / 2.0
~ passTime(12)
~ boatBuildingSpeed = boatBuildingSpeed * 2.0

* [Wait.]

- After many hours of anguishing wait, they start to return. Just a few of them found anything at all, but those who found something have some good information. You can definitely see some opportunities to cause some animosity between the Mages. You thank your fellow Dwellers, feeling reinvigorated.

-> endOfFirstBoatyardVisit

= endOfFirstBoatyardVisit

~ showAllQuests = true

The Elder has something else to say:

"The boat construction is going slower than I hopped. We could make good use of, say, more tools or more wood. Falling trees with our rusted axes isn't very efficient. Please let us know if you discover anything that could help us here."

* "I will, thanks Elder!" ->->


// - BOAT BUILDING HELP -----------------------------------

= tellToolsLocation

"Elder, I heard that there are some tools stashed in a shed in the Yellow Forest."

"Thanks, this will help us to build faster!"

~ boatCompleteness += 0.15

* [Glad to help!] -> boatyardVisit


= tellPitchBarrel

"I have stumbled upon a barrel of pitch. It is on the Snake Creek, at the foot of the Turtle Hill"

"We need pitch so much, thanks!"

~ boatCompleteness += 0.15

* [Be my guest!] -> boatyardVisit


= tellFallenTrees

"Ewing fell many trees on the Yellow Forest, near the Stone Bridge."

"Good, we'll take them!"

~ boatCompleteness += 0.15

* [You are welcome!] -> boatyardVisit


= tellRopes

"I wonder why you didn't take the stock from Quentin's Rope Shop."

"Oh, how could we forget that?! Thanks for bringing this up!"

~ boatCompleteness += 0.15

* [Right!] -> boatyardVisit
