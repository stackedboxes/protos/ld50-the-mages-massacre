=== questTeaForTwo

# CLEAR

Confronting a Mage that has mind-controlling abilities is never a good idea, even for a Free Dweller. But under the ideal circumstances, this might be a little less risky than usual. In particular, a relaxed Mage can't focus his magic power so effectively.

It turns out that Ewing, the Elementalist, loves Aunt Julie's Teahouse, and you know he's going to make a visit there soon. You stand a chance of resisting his mind control well-enough to inculcate him with some bad thoughts about Nikki, the Necromancer.

You thus go to the teahouse, take a seat and wait for Ewing.

~ passTime(1)

He arrives after a short while and takes a seat right across yours. Now is the time.

* [Confront the Elementalist. (Roll <i>willpower</i>)] -> confront
* [Finish your tea and leave -- the plan is too dangerous.] -> leave

= confront

"Mr. Elementalist," you say, "I have some things to tell about a certain Ms. Necromancer. Bad things."

He raises an eyebrow. "Don't you dare disturb my tea time!" You feel the mind control spell starting to affect your intellect. You focus, trying to resist.

- ~ roll("willpower")

-> confrontStakes(true) ->

~ temp resist = 0

{ boughtOutcome1:
    ~ resist += 1
}
{ boughtOutcome2:
    ~ resist += 1
}


{
    - resist == 0:
        You resisted to the mind control itself, but could not keep your mind sharp enough to keep any kind of conversation. You quickly leave your table and move out.
        * [Bummer!]
        -> leave
    - resist == 1:
        You feel dizzy, but still well enough to talk a little bit. You bring up some of the bad past events that happened between the Mages. Your head starts to ache bad, so you leave.
        ~ reduceAgreement(1)
        * [Not bad!]
        -> leave
    - resist == 2:
        You get by very well with the mind control attempt. You start talking about all those popular stories about bad past events that happened between the Mages. Then you bring some ugly stuff from the intel material you got. You see on Ewing's face your efforts were not in vain. You theatrically drink the last sip of your tea and leave.
        ~ reduceAgreement(2)
        * [Good!]
        -> leave
}

= confrontStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [Resist.]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> confrontStakes(false)
    * [Resist more.]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> confrontStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= leave

{ hasEwingVoodooDoll == 1:
    At the teahouse door, you remember you are carrying that voodoo doll. You take it from your bag, turn around and throw it over to the Elementalist. "I found this on Nikki's crypt! Have a nice day."

    You see him breaking his cup.

    ~ reduceAgreement(1)
}

On your way back, you happen to pass through <strong>Quentin's Rope Shop</strong>, which is fully stocked. How come the boat building team didn't take them?

~ bbhRopes = true

* [Return] -> theHub
