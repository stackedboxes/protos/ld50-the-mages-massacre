=== questStonedMeeting

# CLEAR

If your information is correct, Ewing, the Elementalist, and Nikki, the Necromancer, will meet over the Stone Bridge, on the edge of the Yellow Forest. You are not sure what to do there, but sounds like a good opportunity to improvise.

You reach the bridge before the Mages and find an inconspicuous spot under it. After a couple of hours you hear them approaching, each one from one side of the bridge.

~ passTime(2)

They chat for a while, talking about their differences, but also about change, about a new time that is bound to come. Scary talk, given the circumstances. They bid farewell to each other and start to return through the same path each one came from.

Now is the time to act! The Mage closest to you is Ewing, an expert on the earth element. Hitting him with a stone would be an ironic affront. If you throw and hide, he'll think it was Nikki. You take a pebble.

* [Throw the stone! (Roll <i>athletic</i>)] -> throw

= throw

- ~ roll("athletic")

-> throwStakes(true) ->

~temp aim = 0

{ boughtOutcome1:
    ~ aim += 1
}
{ boughtOutcome2:
    ~ aim += 1
}
{ boughtOutcome3:
    ~ aim += 1
}

You throw the pebble and go back to your hideout. The pebble flies in an arch <>

{
    - aim == 0:
        and lands far away from your target. He didn't even notice it.
        * [A missed opportunity!]
        -> theHub
    - aim == 1:
        and lands on Ewing's feet. He turns around, visibly irritated, but after a while resumes his walk.
        ~ reduceAgreement(1)
        * [Better than nothing!]
        -> theHub
    - aim == 2:
        and hits Ewing's back. He turns around, yelling at Nikki: "Is this what you call a new time?!".
        ~ reduceAgreement(2)
        * [Good!]
        -> trees
    - aim == 3:
        and hits Ewing right on his head. A perfect throw! He turns around, infuriated, yelling at Nikki: "Is this what you call a new time?! Go back to your stinky grave, you hypocrite piece of thrash!".
        ~ reduceAgreement(3)
        * [Hurray!]
        -> trees
}

= throwStakes(firstTime)

{ firstTime and pointsAvailable == 0:
    { printZeroRoll() }
    ->->
}
{ printPointsAvailable() }

{ pointsAvailable > 0:
    * [Take aim.]
      ~ boughtOutcome1 = true
      ~ pointsAvailable -= 1
      -> throwStakes(false)
    * [Aim more.]
      ~ boughtOutcome2 = true
      ~ pointsAvailable -= 1
      -> throwStakes(false)
    * [And then aim some more.]
      ~ boughtOutcome3 = true
      ~ pointsAvailable -= 1
      -> throwStakes(false)
    * [I don't want to buy any more outcomes.]
      ~ pointsAvailable = 0
      ->->
}
* { CHOICE_COUNT() == 0 } ->->

= trees

Ewing is so enraged! He raises his hands and mutters some magic words. A large rock by the road raises and then flies in the direction of Nikki. She sidesteps to avoid the projectile, which hits the trees of the Yellow Forest.

Each Mage follows its own way, without further aggressions. You are proud of a job well done! And impressed by Ewing's trick: that rock fell a great number of <strong>trees</strong>, like a giant bowling ball.

~ bbhFallenTrees = true

* [Hmm, fallen trees...] -> theHub
