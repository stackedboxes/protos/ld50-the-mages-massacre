# author: Leandro Motta Barros

INCLUDE state.ink
INCLUDE rolls.ink
INCLUDE hub.ink
INCLUDE globalChecks.ink
INCLUDE boatyardVisit.ink
INCLUDE scoutEwingsEdifice.ink
INCLUDE scoutNecromancersNest.ink
INCLUDE questUndeadArmy.ink
INCLUDE questStonedMeeting.ink
INCLUDE questTeaForTwo.ink
INCLUDE endMassacreAndBoat.ink
INCLUDE endBoatOnly.ink
INCLUDE endMassacreOnly.ink

-> start

=== start

# IMAGE: title.jpg

+ [Start] -> intro
+ [Start (skip intro)] -> theHub
+ [Customize character] -> customizeCharacter
+ [About] -> about

-> intro

=== intro

# CLEAR

For ages, the Two Mages fought for the control of the Island. One of them would wear the Crown for some time, only to be dethroned by the other. This eternal quarrel for power was a source of pain and tears for us, The Free Dwellers -- the few among the people with enough willpower to escape from the Mage's mind control spells.

But this is bound to end. One day, the Mages will wake up with their hair turned green and their eyes turned blue. Then they will agree to talk. Then they will find common ground and come to terms. And then they will get insane and destroy everything on the Island.

So says the <i>Mages Massacre prophecy</i>.

* [No need to worry. Prophecies are just nonsense, right? Riiight?]

- # CLEAR

Well, early this morning the people saw the Mages sporting green hair and blue eyes. Later, the Mages have met near the Yellow Forest and, while there was cursing and swearing, they were not trying to kill each other as they usually do.

* [Damn!]

- The sun has set now, and you join the other Free Dwellers on their secret meeting place to discuss the matter. Desperate, Dwellers speak all at the same time. You can't discern the words, but you know what they are saying:

"The prophecy is true! Soon, the Mages will destroy the Island and kill everyone of us! That's inevitable!"

Only the Elder kept calm: "This is only half-true."

"Uh?!"

"Yes, the Mages will destroy the Island, this is inevitable. But they will not kill us if we are not here."

At this point, it's you who stand and speak.

* "We are not birds, we can't simply fly away from here!"
  "Wings we need not, fellow Dweller."
* "I don't get it."

- Everybody is quiet now, listening to your conversation. The Elder continues: "Let's build a big boat and flee. There must be other islands beyond the Island!"

You look around, people seem to like to idea, but there's a problem everyone is ignoring.

* "There is no time!["] Building a big boat is a big deal!", you say.

- The Elder always has a reply ready: "That's why we need to buy time. If we send one mischievous person to foster conflict among the Mages, we can delay their agreement and hence our destruction."

He briefly stares at the small crowd of Free people, then turns to you again: "And I think you'd be the perfect individual for this role."

Half proud, half ashamed, you accept the job. Next morning, everyone but you will be building a boat.

* [Start your mission] -> theHub

=== customizeCharacter

# CLEAR

On this quirky and irksome screen you can customize your character. And worse: you lose all customizations each time you restart! (Sorry!)

Here are your current skill levels:

Stealth: {skillStealth}
Willpower: {skillWillpower}
Cunning: {skillCunning}
Fighting: {skillFighting}
Athletic: {skillAthletic}

~ temp total = skillStealth + skillWillpower + skillCunning + skillFighting + skillAthletic

{
    - total < 16:
      That's a week character.
    - total > 24:
      That's a strong character.
    - else:
      That's an average character.
}

+ { skillStealth < 10 } [Increase Stealth]
  ~ skillStealth++
  -> customizeCharacter
+ { skillStealth > 1 } [Decrease Stealth]
  ~ skillStealth--
  -> customizeCharacter
+ { skillWillpower < 10 } [Increase Willpower]
  ~ skillWillpower++
  -> customizeCharacter
+ { skillWillpower > 1 } [Decrease Willpower]
  ~ skillWillpower--
  -> customizeCharacter
+ { skillCunning < 10 } [Increase Cunning]
  ~ skillCunning++
  -> customizeCharacter
+ { skillCunning > 1 } [Decrease Cunning]
  ~ skillCunning--
  -> customizeCharacter
+ { skillFighting < 10 } [Increase Fighting]
  ~ skillFighting++
  -> customizeCharacter
+ { skillFighting > 1 } [Decrease Fighting]
  ~ skillFighting--
  -> customizeCharacter
+ { skillAthletic < 10 } [Increase Athletic]
  ~ skillAthletic++
  -> customizeCharacter
+ { skillAthletic > 1 } [Decrease Athletic]
  ~ skillAthletic--
  -> customizeCharacter
+ [Done]
  # CLEAR
  -> start

=== about

# CLEAR

<i>The Mages Massacre</i> was created in 48 hours by Leandro Motta Barros for Ludum Dare 50, whose theme was <i>Delay the inevitable</i>.

I've written it using (and probably abusing) <a href="https:\/\/www.inklestudios.com/ink">Ink</a>. For the awe-inspiring illustration on the title screen, I used <a href="http:\/\/krita.org/">Krita</a>.

+ [OK]
# CLEAR
-> start
