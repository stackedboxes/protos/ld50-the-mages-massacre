# LD 50: The Mages Massacre

My entry for Ludum Dare 50; the theme was "Delay the inevitable".

License is GPL3.

## Credits

* Written in [Ink](https://www.inklestudios.com/ink/). My first time using (and
  probbably abusing) this tool.
* Used [Krita](https://krita.org) for that beautiful title screen picture.

## Notes to self

The Mages:

* Nikki, the Necromancer, The Necromancer's Nest
* Ewing, the Elementalist, Ewing's Edifice

Skills:

* Stealth
* Willpower
* Cunning
* Fighting
* Athletic
